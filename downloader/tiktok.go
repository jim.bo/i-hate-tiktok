package downloader

import (
	"fmt"
	"ihatetiktok/redirdest"
	"os"
	"strings"

	"github.com/89z/mech/tiktok"
)

// GetTiktok takes url and outputs error message and file location
func GetTiktok(url string, filepath string) (*os.File, string) {
	errfile, _ := os.Create(filepath + "error.m4v")
	msg := ""
	var file os.File

	//url := "https://vm.tiktok.com/ZM8nK9kj3/"

	if strings.HasPrefix(url, "https://vm.tiktok.com/") {
		//fmt.Println("finding proper url...")
		url = redirdest.Find(url)
	}
	//fmt.Println(url)

	if strings.HasPrefix(url, "https://m.tiktok.com/") {
		//fmt.Println("finding proper url...")
		url = redirdest.Find(url)
	}

	//fmt.Println(url)

	if strings.HasPrefix(url, "https://www.tiktok.com/") {
		vid, err := tiktok.NewVideo(url)

		if err != nil {
			fmt.Println("invalid link")
			msg = "THE LINK DOESN'T WORK"
			return errfile, msg
		}

		//fmt.Println(url)

		req, err := tiktok.Request(vid)
		if err != nil {
			fmt.Println("invalid request")
			msg = "some sort of an error manipulating url"
			return errfile, msg
		}

		file, err = get(req, vid, filepath)
		if err != nil {
			msg = "some sort of an error downloading video at url"
			return errfile, msg
		}
	}

	return &file, msg
}
