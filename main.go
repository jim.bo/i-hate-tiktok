package main

import (
	"fmt"
	"ihatetiktok/downloader"
	"log"
	"os"
	"strings"
	"time"

	"github.com/joho/godotenv"
	tb "gopkg.in/tucnak/telebot.v2"
	"mvdan.cc/xurls/v2"
)

const filepath string = "./downloaded/"

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	b, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("bot_token"),
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		fmt.Println(err)
		return
	}

	b.Handle(tb.OnText, func(m *tb.Message) {

		xurl, err := xurls.StrictMatchingScheme("https")
		if err != nil {
			fmt.Println(err)
			return
		}

		/*file, msg := downloader.GetTiktok("https://vm.tiktok.com/ZM8nK9kj3/", filepath)

		fmt.Println(file.Name())
		fmt.Println(msg)*/

		furls := xurl.FindAllString(m.Text, -1)

		tik := false
		//ins := false

		for i := 0; i < len(furls); i++ {
			if strings.HasPrefix(furls[i], "https://vm.tiktok.com/") {
				tik = true
			} else if strings.HasPrefix(furls[i], "https://m.tiktok.com/") {
				tik = true
			} else if strings.HasPrefix(furls[i], "https://www.tiktok.com/") {
				tik = true
			} /*else if strings.HasPrefix(furls[i], "https://www.tiktok.com/") {
				ins = true
			}*/

			if tik {
				defer func() {
					if err := recover(); err != nil {
						b.Send(m.Chat, "That looked suspicuous, but I couldn't process it. Still, shame on you for doing such criminal activity! I deleted that message just to be sure")
					} else {
						b.Send(m.Chat, "@"+m.Sender.Username+" posted potentially unwanted content which I at least tried to remove and hopefully here is the linked resource, so others don't have to open shady links. Shame on "+m.Sender.FirstName+" "+m.Sender.LastName)
					}
					b.Delete(m)
				}()
				file, msg := downloader.GetTiktok(furls[i], filepath)

				fmt.Println(file.Name())
				av := &tb.Video{File: tb.FromDisk(file.Name())}

				fmt.Println(msg)

				b.Send(m.Chat, av)
				err = os.Remove(file.Name())
				if err != nil {
					fmt.Println(err)
				}
			}

			tik = false
		}

		/*av := &tb.Video{File: tb.FromDisk(file.Name())}

		fmt.Println("sending")
		b.Send(m.Sender, av)
		fmt.Println("sent")*/
	})

	b.Start()
}
