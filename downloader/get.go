package downloader

import (
	"net/http"
	"os"

	"github.com/89z/mech"
	"github.com/89z/mech/tiktok"
)

func get(req *http.Request, vid tiktok.Video, filepath string) (os.File, error) {
	res, err := mech.RoundTrip(req)
	defer res.Body.Close()

	errfile, _ := os.Create(filepath + "error.m4v")
	if err != nil {
		return *errfile, err
	}
	ext, err := mech.ExtensionByType(res.Header.Get("Content-Type"))
	if err != nil {
		return *errfile, err
	}
	name := vid.Author() + "-" + vid.ID() + ext
	file, err := os.Create(filepath + name + ext)
	if err != nil {
		return *errfile, err
	}
	defer file.Close()
	if _, err := file.ReadFrom(res.Body); err != nil {
		return *errfile, err
	}
	return *file, nil
}
