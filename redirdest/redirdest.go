package redirdest

import (
	"fmt"
	"net/http"
)

func Find(url string) string {
	r, err := http.NewRequest("GET", url, nil)

	if err != nil {
		fmt.Println("RÄJÄHTI")
	}

	r.Header.Set("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0")
	r.Header.Set("Referer", "https://www.tiktok.com/")

	res, err := http.DefaultTransport.RoundTrip(r)

	defer res.Body.Close()

	return res.Header.Get("Location")
}
