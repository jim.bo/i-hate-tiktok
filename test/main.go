package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"

	"github.com/89z/mech/instagram"
)

//https://www.instagram.com/reel/CSzEfInjf8M/?utm_medium=copy_link

// instazucc takes property shortcode and outputs error message and file location
func main() {
	//errfile, _ := os.Create(filepath + "error.m4v")
	//msg := ""
	//var file os.File

	shortcode := "CSzEfInjf8M"
	if !instagram.Valid(shortcode) {
		panic("invalid shortcode")
	}

	var log instagram.Login

	fmt.Println("wat")
	med, err := instagram.GraphQL(shortcode, &log)
	if err != nil {
		panic(err)
	}

	for _, edge := range med.Edges() {
		err := download(edge.URL())
		if err != nil {
			panic(err)
		}
		time.Sleep(100 * time.Millisecond)
	}

}

func download(addr string) error {
	fmt.Println("GET", addr)
	res, err := http.Get(addr)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	par, err := url.Parse(addr)
	if err != nil {
		return err
	}
	file, err := os.Create(path.Base(par.Path))
	if err != nil {
		return err
	}
	defer file.Close()
	if _, err := file.ReadFrom(res.Body); err != nil {
		return err
	}
	return nil
}
